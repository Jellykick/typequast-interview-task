﻿using System;
using System.Collections.Generic;
using System.Linq;
using Typequast_shopping_basket.Entities;
using Typequast_shopping_basket.Services;

namespace Typequast_shopping_basket
{
    class Program
    {
        static void Main(string[] args)
        {
            var availableProducts = new List<Product>();
            var availableDiscounts = new List<Discount>();

            availableProducts.Add(new Product
            {
                Id = 1,
                Name = "Butter",
                Price = 0.80M
            });
            availableProducts.Add(new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1.15M
            });
            availableProducts.Add(new Product
            {
                Id = 3,
                Name = "Bread",
                Price = 1
            });

            availableDiscounts.Add(new Discount
            {
                Id = 1,
                DiscountDescription = "Free milk",
                DiscountCauserId = 2,
                DiscountCauserQuantity = 3,
                DiscountedProductId = 2,
                DiscountedProduct = availableProducts.Single(x => x.Id == 2),
                DiscountPercentage = 100
            });
            availableDiscounts.Add(new Discount
            {
                Id = 2,
                DiscountDescription = "Bread at half the price",
                DiscountCauserId = 1,
                DiscountCauserQuantity = 2,
                DiscountedProductId = 3,
                DiscountedProduct = availableProducts.Single(x => x.Id == 3),
                DiscountPercentage = 50
            });

            ShoppingBasket basket = new ShoppingBasket(new BasketToolsService(new DiscountsService()))
            {
                Products = new List<KeyValuePair<Product, int>>(),
                ApplicableDiscounts = new List<KeyValuePair<Discount, int>>()
            };

            Console.WriteLine("Enter a number for each of the products that can be added into the shopping basket");

            foreach (var product in availableProducts)
            {
                Console.WriteLine(product.Name + ":");
                int productQuantity = Convert.ToInt32(Console.ReadLine());
                if (productQuantity != 0)
                {
                    basket.AddProductToBasket(availableProducts.SingleOrDefault(x => x.Id == product.Id), productQuantity, availableDiscounts);
                }
            }

            basket.CalculateTotalSum();

            basket.LogBasketData();
        }
    }
}
