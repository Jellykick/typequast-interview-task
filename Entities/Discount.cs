﻿namespace Typequast_shopping_basket.Entities
{
    /*Discounter is the product that causes the discount when a certain amount of it is bought.
        If 3 milks give you an extra free milk, then 3 milks are the discounter.*/
    public class Discount
    {
        public int Id { get; set; }
        public string DiscountDescription { get; set; }
        public int DiscountCauserId { get; set; }
        public int DiscountCauserQuantity { get; set; }
        public decimal DiscountPercentage { get; set; }

        public int DiscountedProductId { get; set; }
        public Product DiscountedProduct { get; set; }
    }
}
