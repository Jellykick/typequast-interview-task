﻿using System.Collections.Generic;
using Typequast_shopping_basket.Services;

namespace Typequast_shopping_basket.Entities
{
    public class ShoppingBasket
    {
        private readonly BasketToolsService _basketToolsService;

        public ShoppingBasket(BasketToolsService basketToolsService)
        {
            this._basketToolsService = basketToolsService;
        }

        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public List<KeyValuePair<Product, int>> Products { get; set; }
        public List<KeyValuePair<Discount, int>> ApplicableDiscounts { get; set; }

        public void AddProductToBasket(Product product, int productQuantity, List<Discount> allDiscounts)
        {
            _basketToolsService.AddProductToBasket(this, product, productQuantity, allDiscounts);
        }

        public void CalculateTotalSum()
        {
            _basketToolsService.CalculateTotalSum(this);
        }

        public void LogBasketData()
        {
            _basketToolsService.LogBasketData(this);
        }
    }
}