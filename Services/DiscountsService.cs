﻿using System.Collections.Generic;
using System.Linq;
using Typequast_shopping_basket.Entities;

/*The discount calculation method is pulled into it's own service because this way, in case the discount system needs to be upgraded for supporting a more complex type of discount,
 only the Discount class and Discounts service would need to be changed.  This way, SOLID principles are respected and other classes remain untouched*/
namespace Typequast_shopping_basket.Services
{
    public class DiscountsService
    {
        public List<KeyValuePair<Discount, int>> FindApplicableDiscounts(Product product, int productQuantity, List<Discount> allDiscounts)
        {
            var potentiallyApplicableDiscounts = allDiscounts.Where(x =>
                    x.DiscountCauserId == product.Id)
                .Select(x => x).ToList();

            var applicableDiscounts = new List<KeyValuePair<Discount, int>>();

            if (potentiallyApplicableDiscounts.Count != 0)
            {
                foreach (var discount in potentiallyApplicableDiscounts)
                {
                    /*The if-else block is here because a 'free item' type of discount
                     can't follow the same rules as the 'take some money of an item' type of discount.
                    If a user bought 7 milks, the (one free milk for every pack od 3) discount 
                    is used only once even considering it was available twice.*/

                    var usedDiscounts = 0;
                    var availableDiscounts = productQuantity / discount.DiscountCauserQuantity;

                    if (discount.DiscountPercentage == 100)
                    {
                        if (availableDiscounts > 0)
                        {
                            for (int i = 0; i < productQuantity - discount.DiscountCauserQuantity; i += discount.DiscountCauserQuantity)
                            {
                                usedDiscounts++;
                                i++;
                            }
                            applicableDiscounts.Add(new KeyValuePair<Discount, int>(discount, usedDiscounts));
                        }
                    }
                    else
                    {
                        applicableDiscounts.Add(new KeyValuePair<Discount, int>(discount, availableDiscounts));
                    }
                }
            }
            return applicableDiscounts;
        }
    }
}
