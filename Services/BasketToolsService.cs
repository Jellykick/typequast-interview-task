﻿using System;
using System.Collections.Generic;
using System.Linq;
using Typequast_shopping_basket.Entities;

namespace Typequast_shopping_basket.Services
{
    public class BasketToolsService
    {
        private readonly DiscountsService _discountsService;

        public BasketToolsService(DiscountsService discountsService)
        {
            this._discountsService = discountsService;
        }

        public void AddProductToBasket(ShoppingBasket basket, Product product, int productQuantity, List<Discount> allDiscounts)
        {
            basket.Products.Add(new KeyValuePair<Product, int>(product, productQuantity));
            basket.ApplicableDiscounts.AddRange(_discountsService.FindApplicableDiscounts(product, productQuantity, allDiscounts));
        }

        public void CalculateTotalSum(ShoppingBasket basket)
        {
            var totalWithNoDiscounts = basket.Products.Sum(x => x.Key.Price * x.Value);

            foreach (var discount in basket.ApplicableDiscounts)
            {
                if (basket.Products.Any(x => x.Key.Id == discount.Key.DiscountedProductId))
                {
                    var productToBeDiscounted = basket.Products.Single(x => x.Key.Id == discount.Key.DiscountedProductId);
                    var fullProductPrice = productToBeDiscounted.Key.Price;

                    var usableDiscounts = discount.Value;
                    if (discount.Value > productToBeDiscounted.Value)
                    {
                        usableDiscounts = productToBeDiscounted.Value;
                    }

                    totalWithNoDiscounts -= (fullProductPrice * discount.Key.DiscountPercentage / 100) * usableDiscounts;
                }
            }

            basket.TotalPrice = totalWithNoDiscounts;
        }

        public void LogBasketData(ShoppingBasket basket)
        {
            Console.WriteLine();
            Console.WriteLine("You basket contains these items: ");
            Console.WriteLine();

            foreach (var product in basket.Products)
            {
                Console.WriteLine(product.Value + " * " + product.Key.Name + " with a cost of: " + product.Key.Price + "$ per item");
            }

            Console.WriteLine();

            if (basket.ApplicableDiscounts.Any(x => x.Value != 0))
            {
                Console.WriteLine("And these discounts: ");
                Console.WriteLine();
                foreach (var discount in basket.ApplicableDiscounts)
                {
                    Console.WriteLine(discount.Value + " * " + "'" + discount.Key.DiscountDescription + "':" + 
                                      " which reduces the price of one " + discount.Key.DiscountedProduct.Name +
                                      " " + discount.Key.DiscountPercentage + "%");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Making the total cost of your basket: " + basket.TotalPrice + "$");
            Console.WriteLine("Discounts are applied only if you bought the products that they can be applied to.");
        }
    }
}
